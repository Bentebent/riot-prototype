﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
public class main : MonoBehaviour 
{
	List<DimmingLight> m_lights;

	// Use this for initialization
	void Start () 
	{
		m_lights = new List<DimmingLight>();
		LoadScenePrefabs();
		CreateEntities();
	}

	void OnApplicationQuit()
	{
	}

	void LoadScenePrefabs()
	{
		//this is slow as fuck
		DimmingLight[] dl;
		dl = Resources.FindObjectsOfTypeAll<DimmingLight>();

		for (int i = 0; i < dl.GetLength(0); i++)
		{
			//Make sure it is actually a game object and not the prefab itself
			if (dl[i].gameObject.activeInHierarchy)
			{
				m_lights.Add(dl[i]);
				//just for testing
				m_lights[m_lights.Count - 1].m_isPrefab = true;
			}
		}
	}

	void CreateEntities()
	{
		//Create an entity of the same type as the prefab without using the editor
		print ("trying to add light");

		GameObject g = Instantiate(Resources.Load("Spinning Light"), new Vector3(-2.5f, 1.0f, -3.5f), Quaternion.identity) as GameObject;
		g.gameObject.tag = "new";

		DimmingLight[] dl;
		dl = Resources.FindObjectsOfTypeAll<DimmingLight>();


		for (int i = 0; i < dl.GetLength(0); i++)
		{
			//Make sure it is actually a game object and not the prefab itself
			if (dl[i].gameObject.activeInHierarchy && dl[i].gameObject.tag == "new")
			{
				m_lights.Add(dl[i]);
				//just for testing
				m_lights[m_lights.Count - 1].gameObject.tag = "Untagged";
			}
		}

		print ("huehue");


		//l = (DimmingLight)Instantiate(Resources.Load("Spinning Light"), new Vector3(3.0f, 1.0f, -3.0f), Quaternion.identity);
		//m_lights.Add(l);
	}

	// Update is called once per frame
	void Update () 
	{
		foreach(DimmingLight l in m_lights)
		{
			if (l.m_isPrefab)
				l.light.color = Color.red;
			else
			{
				l.light.color = Color.green;
			}
		}
	}

	void Revert() 
	{
		foreach(DimmingLight l in m_lights)
			UnityEditor.PrefabUtility.ResetToPrefabState(l);
	}
}
