﻿using UnityEngine;
using System.Collections;

public class DimmingLight : MonoBehaviour 
{
	public float m_dimming;
	public bool m_isPrefab;
	public Color m_originalColor;

	// Use this for initialization
	void Start () 
	{
		m_originalColor = this.light.color;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (this.light.intensity > 3 || this.light.intensity <= 0) 
		{
			m_dimming *= -1.0f;
		}

		this.light.intensity -= m_dimming * Time.deltaTime;
	}
}
